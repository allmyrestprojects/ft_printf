cmake_minimum_required(VERSION 3.14)
project(ft_printf)

set(CMAKE_CXX_STANDARD 14)

add_executable(ft_printf
        main.c ft_printf.c ft_printf.h libutils.c longdiv.c)
