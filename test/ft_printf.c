//
// Created by Bears Gerda on 24/11/2019.
//
#include "ft_printf.h"

void    get_double(double num, t_double *var)
{
    d.dl = num;

    var->counter = 0;
    var->tmp = 0;
    var->s = ( d.dw >> 63 ) ? -1 : 1;
    var->e = ( d.dw >> 52 ) & 0x7FF;
    var->m = var->e ? ( d.dw & 0xFFFFFFFFFFFFF ) | 0x10000000000000 : ( d.dw & 0xFFFFFFFFFFFFF ) << 1;
}

void    init(t_bin *bin)
{
    int i;

    i = 0;
    while(i < 1000)
    {
        bin->strbin[i] = 0;
        bin->intbin[i] = 0;
        i++;
    }
    bin->strbin[0] = '1';
    bin->strbin[1] = '.';
    bin->intbin[0] = 1;
}

void    get_binary(t_double var, t_bin *bin)
{
    int i;

    i = 0;
    if(var.m / 2 != 0)
    {
        var.counter++;
        var.tmp = var.m % 2;
        var.m = var.m / 2;
        get_binary(var, bin);
        while(bin->strbin[i] != 0)
            i++;
        bin->strbin[i] = var.tmp + '0';
        bin->intbin[i - 1] = var.tmp;
    }
    bin->shift = var.e - 1023;
}

void    ft_print(char *str)
{
    if (*(++str))
        ft_print(str);
    ft_putchar(*(--str));
}

void    frac(t_double var, int precision)
{
    int tmp;
    int tmp1;
    int counter;
    int i;
    char num1[1000];
    char num2[1000];
    char num3[1000];
    char num4[1000];
    char res[1000];

    i = 51;
    tmp = 0;
    tmp1 = -1;
    while(var.m / 2 != 0)
    {
        if (var.m % 2 == 1 && i >= var.e - 1023)
        {
            counter = i  - (var.e - 1023) + 1;
            if (tmp == 0)
            {
                get_mod(-counter, num1, "5");
                tmp = counter;
            }
            else
            {
                get_mod(-counter, num2, "5");
                fill_zeros(num1, num2, res);
                add(num1, num2, res);
                ft_strncpy(num1, res, ft_strlen(res) + 1);
            }
        }
        else if (var.m % 2 == 1 && i != var.e - 1023)
        {
            counter = -(i - (var.e - 1023) + 1);
            if (tmp1 == -1)
            {
                get_mod(counter, num3, "2");
                tmp1 = counter;
            }
            else
            {
                get_mod(counter, num4, "2");
                add(num4, num3, res);
                ft_strncpy(num3, res, ft_strlen(res) + 1);
            }
        }
        var.m = var.m / 2;
        i--;
    }
    if (var.e - 1023 < 0)
    {
        ft_putchar('0');
        ft_putchar('.');
        counter = -(var.e - 1023);
        get_mod(-counter, num2, "5");
        fill_zeros(num1, num2, res);
        add(num1, num2, res);
        ft_strncpy(num1, res, ft_strlen(res) + 1);
    }
    else
    {
        counter = var.e - 1023;
        get_mod(counter, num4, "2");
        add(num4, num3, res);
        ft_strncpy(num3, res, ft_strlen(res) + 1);
        ft_print(num3);
        ft_putchar('.');
    }
    ft_print(num1);
}

int ft_printf(const char *format, ...)
{
	const char  *traverse;
	double      num;
    t_double    var;
	va_list arg;
	va_start(arg, format);

	traverse = format;
	while (*traverse != '\0')
	{
		while(*traverse != '%')
		{
			ft_putchar(*traverse);
			traverse++;
		}
		traverse++;
		if (*traverse == 'f')
		{
            num = va_arg(arg, double);
            get_double(num, &var);
            frac(var, 6);
        }
		traverse++;
	}
	va_end(arg);
	return (1);
}