//
// Created by Bears Gerda on 24/11/2019.
//

#ifndef FT_PRINTF_FT_PRINTF_H
#define FT_PRINTF_FT_PRINTF_H

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>



union {
    double      dl;
    uint64_t    dw;
} d;

typedef struct      s_bin
{
    int             shift;
    char            strbin[1000];
    int             intbin[1000];
}                   t_bin;

typedef struct      s_double
{
    int             s;
    int             e;
    unsigned long   m;
    int             tmp;
    int             counter;
}                   t_double;



int     ft_printf(const char *format, ...);
int     smul(const char *a, char *c, int sym, int size);
void    get_mod(int size, char *c, char *a);
void    add(char *a, char *b, char *c);
void    fill_zeros(char *a, char *b, char *tmp);
void    get_double(double num, t_double *var);


size_t	ft_strlen(const char *s);
void    strzeros(char *str);
char	*ft_strncpy(char *dst, const char *src, size_t len);
char	*ft_strdup(const char *s1);
void	ft_putchar(char c);
char	*ft_strnew(size_t size);

#endif
