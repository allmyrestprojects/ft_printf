//
// Created by ai on 28.11.19.
//

#include "ft_printf.h"

size_t	ft_strlen(const char *s)
{
    size_t	i;

    i = 0;
    while (s[i] != '\0')
        i++;
    return (i);
}

void    strzeros(char *str)
{
    int i;

    i = -1;
    while(str[++i] != '\0')
        str[i] = 0;
}

void	ft_putchar(char c)
{
    write(1, &c, 1);
}

void	*ft_memset(void *b, int c, size_t len)
{
    size_t			i;
    unsigned char	value;
    char			*str;

    value = (unsigned char)c;
    str = (char *)b;
    i = 0;
    while (i < len)
    {
        str[i] = value;
        i++;
    }
    return (b);
}

void	*ft_memalloc(size_t size)
{
    void	*m;

    m = malloc(size);
    if (m == NULL)
        return (NULL);
    return (ft_memset(m, 0, size));
}


char	*ft_strnew(size_t size)
{
    return ((char*)ft_memalloc(size + 1));
}


char	*ft_strncpy(char *dst, const char *src, size_t len)
{
    size_t	i;

    i = 0;
    while (src[i] != '\0' && i < len)
    {
        dst[i] = src[i];
        i++;
    }
    while (i < len)
    {
        dst[i] = '\0';
        i++;
    }
    return (dst);
}


char	*ft_strdup(const char *s1)
{
	char	*str;
	int		len;

	len = 0;
	while (s1[len] != '\0')
		len++;
	if (!(str = (char*)malloc(sizeof(*str) * (len + 1))))
		return (NULL);
	len = 0;
	while (s1[len] != '\0')
	{
		str[len] = s1[len];
		len++;
	}
	str[len] = '\0';
	return (str);
}
